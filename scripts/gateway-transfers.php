<?php

namespace Script\Gateway\Transfers;

use App;
use App\Model\Repository\GatewayRepository;
use App\Model\EmailService;

# Autoload of classes with Composer - and also of our Bootstrap class
require __DIR__ . '/../vendor/autoload.php';

# Configurator will compose DI container for us
$container = App\Bootstrap::boot()
	->createContainer();

# We can fetch what we need from it
$gatewayRepository = $container->getByType(GatewayRepository::class);
$emailService = $container->getByType(EmailService::class);


$debugMode = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : 0;

$bankHash = $gatewayRepository->getBankHash();

/*
call the following URL for resetting the date in the FIO API:
https://www.fio.cz/ib_api/rest/set-last-date/ $bankHash /2000-01-01/
*/


$json = file_get_contents('https://www.fio.cz/ib_api/rest/last/' . $bankHash . '/transactions.json');

$data = json_decode($json, true);


$emailService->setTemplate(EmailService::TEMPLATES[GatewayRepository::TYPE_THANKS]);

$transactions = $data['accountStatement']['transactionList']['transaction'];

foreach ($transactions as $transaction) {
	$payment = [
		'amount' => isset($transaction['column1']) ? floatval($transaction['column1']['value']) : 0,
		'currency' => isset($transaction['column14']) ? $transaction['column14']['value'] : '',
		'transactionId' => isset($transaction['column5']) ? intval($transaction['column5']['value']) : '',
		'bankTransactionId' => isset($transaction['column22']) ? $transaction['column22']['value'] : '',
		'paymentType' => 'funds_transfer',
		'ss' => isset($transaction['column6']) ? $transaction['column6']['value'] : ''
	];
	$personal = [
		'accountNo' => isset($transaction['column2']) ? $transaction['column2']['value'] : '',
		'bankCode' => isset($transaction['column3']) ? $transaction['column3']['value'] : '',
		'accountName' => isset($transaction['column10']) ? $transaction['column10']['value'] : ''
	];


	if ($payment['amount'] > 0) {

		if ($gatewayRepository->isAlreadyProcessed($payment['transactionId'], $payment['bankTransactionId'])) continue; // check whether it has already been processed or not

		if ($gatewayRepository->wasPromised($payment['transactionId'])) { // payment was promised based on variable symbol

			$returned = $gatewayRepository->insertPromised($payment, $personal);

			$email = $returned['email'];

			$emailService->setParams(
				['subject' => EmailService::TITLES[GatewayRepository::TYPE_THANKS]]
			);

			$emailService->setReplyTo('info@srnka.net');

			$emailService->send($email);

			if ($debugMode) echo "YES - " . $payment['transactionId'] . " - " . $email . "\n";

		} else { // payment was NOT promised

			$gatewayRepository->insertNotPromised($payment, $personal);

			if ($debugMode) echo "NO - " . $payment['transactionId'] . "\n";

		}

	}
}

if ($debugMode) {
	$runTime = (microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
	echo 'execution time: ' . floor($runTime) . "s " . number_format(($runTime - floor($runTime)) * 1000, 0) . "ms\n";
}

/*
wierd bank data naming:
column22			=> ID pohybu										=> unikátní číslo pohybu - 10 numerických znaků
column0				=> Datum												=> datum pohybu ve tvaru rrrr-mm-dd+GMT
column1				=> Objem												=> velikost přijaté (odeslané) částky
column14			=> Měna													=> měna přijaté (odeslané) částky dle standardu ISO 4217
column2				=> Protiúčet										=> číslo protiúčtu
column10			=> Název protiúčtu							=> název protiúčtu
column3				=> Kód banky										=> číslo banky protiúčtu
column12			=> Název banky									=> název banky protiúčtu
column4				=> KS														=> konstantní symbol
column5				=> VS														=> variabilní symbol
column6				=> SS														=> specifický symbol
column7				=> Uživatelská identifikace			=> uživatelská identifikace
column16			=> Zpráva pro příjemce					=> zpráva pro příjemce
column8				=> Typ pohybu										=> typ operace
column9				=> Provedl											=> oprávněná osoba, která zadala příkaz
column18			=> Upřesnění										=> upřesňující informace
column25			=> Komentář											=> Komentář
column26			=> BIC													=> bankovní identifikační kód banky protiúčtu dle standardu ISO 9362
column17			=> ID pokynu										=> číslo příkazu
column27			=> Reference plátce							=> bližší identifikace platby dle ujednání mezi účastníky platby
*/