<?php

namespace Script\Gateway\Reminder;

use App;
use App\Model\Repository\GatewayRepository;
use App\Model\EmailService;

# Autoload of classes with Composer - and also of our Bootstrap class
require __DIR__ . '/../vendor/autoload.php';

# Configurator will compose DI container for us
$container = App\Bootstrap::boot()
	->createContainer();

# We can fetch what we need from it
$gatewayRepository = $container->getByType(GatewayRepository::class);
$emailService = $container->getByType(EmailService::class);


$debugMode = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : 0;

$qrCodeUrl = 'https://api.paylibo.com/paylibo/generator/czech/image?compress=false&size=150&accountNumber=%s&bankCode=%s&amount=%s&currency=CZK&vs=%s&message=%s';

$info = $gatewayRepository->getInfo();

$now = $gatewayRepository->getTime(); //we need to have a global time for the whole script

$emailService->setTemplate(EmailService::TEMPLATES[GatewayRepository::TYPE_REMINDER]);

// payments once

$promises = $gatewayRepository->getReminderData('gateway_once', $now);

foreach ($promises as $promise) {
	$emailService->setParams([
		'frequency' => 'once',
		'vs' => $promise['transactionId'],
		'bankAccountNumber' => $info['bankAccountNumber'],
		'bankAccountCode' => $info['bankAccountCode'],
		'amount' => $promise['amount'],
		'qrCode' => sprintf($qrCodeUrl,
        $info['bankAccountNumber'],
        $info['bankAccountCode'],
        $promise['amount'],
        $promise['transactionId'],
        urlencode($info['paymentMessage'])),
		'url' => 'srnka.net',
		'hash' => $promise['hash'],
		'subject' => EmailService::TITLES[GatewayRepository::TYPE_REMINDER]
	]);

	$emailService->setReplyTo('info@srnka.net');

	$emailService->send($promise['email']);

	if ($debugMode) echo $promise['email'] . "\n";
}

// payments monthly

$promises = $gatewayRepository->getReminderData('gateway_monthly', $now);

foreach ($promises as $promise) {
	$emailService->setParams([
		'frequency' => 'monthly',
		'vs' => $promise['transactionId'],
		'bankAccountNumber' => $info['bankAccountNumber'],
		'bankAccountCode' => $info['bankAccountCode'],
		'amount' => $promise['amount'],
		'qrCode' => sprintf($qrCodeUrl,
        $info['bankAccountNumber'],
        $info['bankAccountCode'],
        $promise['amount'],
        $promise['transactionId'],
        urlencode($info['paymentMessage'])),
		'url' => 'srnka.net',
		'hash' => $promise['hash'],
		'subject' => EmailService::TITLES[GatewayRepository::TYPE_REMINDER]
	]);

	$emailService->setReplyTo('info@srnka.net');

	$emailService->send($promise['email']);

	if ($debugMode) echo $promise['email'] . "\n";

}

if ($debugMode) {
	$runTime = (microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
	echo 'execution time: ' . floor($runTime) . "s " . number_format(($runTime - floor($runTime)) * 1000, 0) . "ms\n";
}
