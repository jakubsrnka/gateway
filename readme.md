Requirements
------------

- Web Project for Nette 3.0 requires PHP 7.1


Installation
------------

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/web-project path/to/install
	cd path/to/install


Make directories `temp/` and `log/` writable.


Web Server Setup
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.

For Apache or Nginx, setup a virtual host to point to the `www/` directory of the project and you
should be ready to go.

**It is CRITICAL that whole `app/`, `log/` and `temp/` directories are not accessible directly
via a web browser. See [security warning](https://nette.org/security-warning).**

# Install dependencies
Note: after installing composer dependencies in docker, vendor folder is owned by root, you need to chown it
```
docker-compose up --build composer
chown $USER:$USER -R vendor
```

# Local dev
```
docker-compose up
```

Purge local dev db
```
docker volume rm gateway_databases
docker exec -it gateway_mysql_1 bash
mysql -prootpass < /docker-entrypoint-initdb.d/0.sql
mysql -prootpass < gateway < /docker-entrypoint-initdb.d/gateway.sql

```

### Phpmyadmin
go to http://localhost:8081/ and fill in 
server: mysql
username: gateway
password: heslo123

# Used libraries
### Styling 
http://lesscss.org/

# Sources
RUIAN https://www.cuzk.cz/Uvod/Produkty-a-sluzby/RUIAN/2-Poskytovani-udaju-RUIAN-ISUI-VDP/Ciselniky-ISUI/Nizsi-uzemni-prvky-a-uzemne-evidencni-jednotky/Obce.aspx?feed=RSS
https://vdp.cuzk.cz/vdp/ruian/okresy/export?vc.kod=&kr.kod=&ok.nazev=&ok.kod=&ohrada.id=&okg.sort=UZEMI&export=CSV
https://nahlizenidokn.cuzk.cz/StahniAdresniMistaRUIAN.aspx


# Crons

optional parameter ```1``` for debug mode
```
scripts/gateway-reminder.php
scripts/petition-reminder.php
```