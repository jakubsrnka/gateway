<?php

namespace App\Model;

class Request {
  /** @var \Nette\Http\Request */
  private $httpRequest;

  public function __construct(\Nette\Http\Request $httpRequest)
  {
    $this->httpRequest = $httpRequest;
  }

  /**
   * Lets get ip of client behind cloudflare
   *
   * @return string
   */
  public function getClientAddress(): string
  {
    return $this->httpRequest->getRemoteAddress();
  }
}
