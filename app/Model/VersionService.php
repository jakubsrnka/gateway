<?php


namespace App\Model;

use Tracy\Debugger;

class VersionService
{
  private $version;

  public function __construct(string $version)
  {
    $this->version = Debugger::$productionMode ? $version : rand();
  }

  /**
   * @return string
   */
  public function getVersion(): string
  {
    return $this->version;
  }
}
