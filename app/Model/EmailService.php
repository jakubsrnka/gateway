<?php


namespace App\Model;

use App\Model\Repository\GatewayRepository;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Bridges\ApplicationLatte\UIMacros;
use Nette\Application\LinkGenerator;

class EmailService
{
  /** @var ILatteFactory */
  private $latteFactory;

  /** @var LinkGenerator */
  private $linkGenerator;

  private $mailer;

  /** @var array */
  private $params;

  /** @var string */
  private $mailTemplate;

  /** @var string */
  private $replyTo;


  // Todo: private and public signatories could have different template emails
  public const TEMPLATES = [
    GatewayRepository::TYPE_CONFIRM => '/../Presenters/templates/Email/gateway-confirm.latte',
    GatewayRepository::TYPE_REMINDER => '/../Presenters/templates/Email/gateway-reminder.latte',
    GatewayRepository::TYPE_THANKS => '/../Presenters/templates/Email/gateway-thanks.latte'
  ];

  public const TITLES = [
    GatewayRepository::TYPE_CONFIRM => 'Dokončete Váš dar, zbývá poslední krok',
    GatewayRepository::TYPE_REMINDER => 'Zapomněli jste potvrdit Váš dar?',
    GatewayRepository::TYPE_THANKS => 'Děkujeme za Vaši štědrost a zapojení!'
  ];


  public function __construct(ILatteFactory $latteFactory, LinkGenerator $linkGenerator, IMailer $mailer)
  {
    $this->latteFactory = $latteFactory;
    $this->linkGenerator = $linkGenerator;
    $this->mailer = $mailer;
  }

  public function send(string $receiver, $debug = false)
  {

    $mail = new Message();
    $MAIL_FROM = 'Demo Darovací Brána <notifications@srnka.net>';
    $mail->setFrom($MAIL_FROM);
    $mail->setSubject($this->params['subject']);
    $mail->setHtmlBody($this->getHtml());
    $mail->setHeader('X-CampaignID', 'GsYLqMZZY7Yyn9nLPBcJ');

    if ($this->replyTo) {
      $mail->addReplyTo($this->replyTo);
    }
    $mail->addTo($receiver);

    if($debug) {
      dump($mail);
      echo $mail->getHtmlBody();
    } else {
      $this->mailer->send($mail);
    }
  }

  function getHtml()
  {
    $latte = $this->latteFactory->create();

    $latte->addProvider('uiControl', $this->linkGenerator);


    UIMacros::install($latte->getCompiler());

    $htmlContent = $latte->renderToString(__DIR__ . $this->mailTemplate , $this->params);

    return $htmlContent;
  }

  public function setParams(array $params)
  {
    $this->params = $params;
  }

  function setTemplate($template)
  {

    $this->mailTemplate = $template;
  }

  public function setReplyTo($replyTo)
  {
    $this->replyTo = $replyTo;
  }
}

