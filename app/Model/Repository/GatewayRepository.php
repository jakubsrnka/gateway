<?php


namespace App\Model\Repository;

use Nette\Database\Context;
use Tracy\ILogger;
use App\Model\EmailService;
use Nette\Http\Request as HttpRequest;
use Nette\Utils\Random;
use Tracy\Debugger;

class GatewayRepository
{

  // table names variables
  private $table_config = 'gateway_config';
  private $table_personal = 'gateway_personal';
  private $table_once = 'gateway_once';
  private $table_monthly = 'gateway_monthly';
  private $table_accounts = 'gateway_accounts';
  private $table_personal_accounts = 'gateway_personal_accounts';
  private $table_recieved = 'gateway_recieved';
  private $table_nopromise = 'gateway_nopromise';

  private $table_ruian = 'obce';

  private $urlFormatQr = 'https://api.paylibo.com/paylibo/generator/czech/image?compress=false&size=150&accountNumber=%s&bankCode=%s&amount=%s&currency=CZK&vs=%s&message=%s';

  // constants
  public const GATEWAY_FREQUENCY_ONCE = 'once';
  public const GATEWAY_FREQUENCY_MONTHLY = 'monthly';

  public const TYPE_CONFIRM = 1;
  public const TYPE_REMINDER = 2;
  public const TYPE_THANKS = 3;

  public const GATEWAY_TRANSACTION_ID_ONCE_MIN = 1000000000;
  public const GATEWAY_TRANSACTION_ID_MONTHLY_MIN = 5500000000;

  /**
   * @var Context
   */
  public $db;

  /** @var ILogger */
  private $logger;

  /** @var EmailService @inject */
  public $emailService;

  /** @var HttpRequest */
  private $httpRequest;


  function __construct(Context $db, ILogger $logger, EmailService $emailService, HttpRequest $httpRequest)
  {
    $this->db = $db;
    $this->logger = $logger;
    $this->emailService = $emailService;
    $this->httpRequest = $httpRequest;
  }

  // generator for random Uuid
  private function getUuid(): string
  {
    return Random::generate(25);
  }

  // backend data validation
  public function validateData(array $json, $ip)
  {
    if ($json) {

      $info = $this->getInfo();

      // personal info validation
      if (isset($json['personal'])) {
        $personal = $json['personal'];
        if (empty($personal['fname'])
          || empty($personal['lname'])
          || !preg_match('/^((\+|00)42[01])?([\s+\-()x]*\d){6,}$/', $personal['phone'])
          || !preg_match('/^[a-zA-Z0-9.!#$%&*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/', $personal['email'])) {
          return ["status" => "error1"];
        }
      } else {
        return ["status" => "error2"];
      };

      // address validation
      if (isset($json['address'])) {
        $address = $json['address'];
        if (empty($address['street'])
          || !preg_match('/^[eE]?[0-9 \/]{1,}[a-zA-Z]?$/', $address['houseNumber'])
          || !is_numeric($address['obecId'])
          || !preg_match('/^[0-9]{3}\ [0-9]{2}|[0-9]{5}$/', $address['postcode'])) {
          return ["status" => "error3"];
        }
      } else {
        return ["status" => "error4"];
      };

      // payment info validation
      if (isset($json['payment'])) {
        $payment = $json['payment'];
        if (!in_array($payment['frequency'], [self::GATEWAY_FREQUENCY_ONCE, self::GATEWAY_FREQUENCY_MONTHLY])
          || !($payment['amount'] > 0)) {
          return ["status" => "error5"];
        }
      } else {
        return ["status" => "error6"];
      };

      // inserting or updating the db
      $update = [
        'fname' => $personal['fname'],
        'lname' => $personal['lname'],
        'phone' => $personal['phone'],
        'street' => $address['street'],
        'houseNumber' => $address['houseNumber'],
        'obecId' => $address['obecId'],
        'postcode' => $address['postcode'],
        'ip' => $ip
      ];
      
      $this->db->query('INSERT INTO ' . $this->table_personal, [
        'fname' => $personal['fname'],
        'lname' => $personal['lname'],
        'phone' => $personal['phone'],
        'email' => $personal['email'],
        'street' => $address['street'],
        'houseNumber' => $address['houseNumber'],
        'obecId' => $address['obecId'],
        'postcode' => $address['postcode'],
        'ip' => $ip
      ], 'ON DUPLICATE KEY UPDATE', $update);

      // getting id of user
      $id = $this->db->table($this->table_personal)->select('id')->where('email', $personal['email'])->fetchField();

      // logging all incoming data for future record
      $this->logger->log(print_r([
        'id' => $id,
        'fname' => $personal['fname'],
        'lname' => $personal['lname'],
        'phone' => $personal['phone'],
        'email' => $personal['email'],
        'street' => $address['street'],
        'houseNumber' => $address['houseNumber'],
        'obecId' => $address['obecId'],
        'postcode' => $address['postcode'],
        'ip' => $ip
      ], true), 'gateway');

      // setting new hash
      $hash = $this->getUuid(25);

      // which table depending on frequency
      $table_payment = ($payment['frequency'] == self::GATEWAY_FREQUENCY_ONCE) ? $this->table_once : $this->table_monthly;

      // getting transaction id (variable symbol)
      $transactionId = $this->db->table($table_payment)->select('transactionId')->order('id DESC')->limit('1')->fetchField() + 1;
      if ($transactionId == 1) $transactionId = ($payment['frequency'] == self::GATEWAY_FREQUENCY_ONCE) ? self::GATEWAY_TRANSACTION_ID_ONCE_MIN : self::GATEWAY_TRANSACTION_ID_MONTHLY_MIN;

      $ourPayment = [
        'amount' => $payment['amount'],
        'currency' => 'CZK',
        'transactionId' => $transactionId,
        'recieved' => 0,
        'giver' => $id,
        'hash' => $hash
      ];

      // inserting data into db
      $this->db->table($table_payment)->insert($ourPayment);

      // loggig for future record
      $this->logger->log(print_r($ourPayment, true), 'gateway');

      // tell frontend to keep user here
      $destination = "here";

      // formatting the QR code url
      $url = sprintf($this->urlFormatQr,
        $info['bankAccountNumber'],
        $info['bankAccountCode'],
        $payment['amount'],
        $transactionId,
        urlencode($info['paymentMessage']));

      // sending email via EmailService
      $this->emailService->setTemplate(EmailService::TEMPLATES[self::TYPE_CONFIRM]);
      $this->emailService->setParams([
        'frequency' => $payment['frequency'],
        'vs' => $transactionId,
        'bankAccountNumber' => $info['bankAccountNumber'],
        'bankAccountCode' => $info['bankAccountCode'],
        'amount' => $payment['amount'],
        'qrCode' => $url,
        'hash' => $hash,
        'subject' => EmailService::TITLES[self::TYPE_CONFIRM]
      ]);
      $this->emailService->setReplyTo('brana@srnka.net');
      if ($destination == "here") $this->emailService->send($personal['email']);


      return ["status" => "ok", "destination" => $destination, "url" => $url, "frequency" => $payment['frequency'], "variableSymbol" => $transactionId, "hash" => $hash, "amount" => $payment['amount']];
    }
    return 'not ok';
  }


  public function getInfo($location = null)
  {
    $info['bankAccountNumber'] = $this->db->table($this->table_config)->select('content')->where('type', 'bankAccountNumber')->fetchField();
    $info['bankAccountCode'] = $this->db->table($this->table_config)->select('content')->where('type', 'bankAccountCode')->fetchField();
    $info['paymentMessage'] = $this->db->table($this->table_config)->select('content')->where('type', 'paymentMessage')->fetchField();
    return $info;
  }

  // set payment as done so we do not spam people with reminder emails
  public function setAsDone($id, $hash)
  {
    // which table
    $table = $this->tableMonthlyOrOnce($id);

    // set as done
    $this->db->table($table)->where(['transactionId' => $id, 'hash' => $hash])->update(['done' => 1]);

    // did we really set it as done, if not, log
    if ($this->db->table($table)->select('done')->where(['transactionId' => $id, 'hash' => $hash])->fetchField() != 1) {

      $this->logger->log('set as done - transactionId and hash do not match - "transactionId" => ' . $id . ', "hash" => ' . $hash, 'gatewayError');

      return false;

    }

    return true;
  }

  // function for selecting a table
  public function tableMonthlyOrOnce($id)
  {
    if ($id < self::GATEWAY_TRANSACTION_ID_MONTHLY_MIN && $id >= self::GATEWAY_TRANSACTION_ID_ONCE_MIN) { // once
      return $this->table_once;
    } else if ($id >= self::GATEWAY_TRANSACTION_ID_MONTHLY_MIN){ // monthly
      return $this->table_monthly;
    }
  }

  // function for getting frequency from transactionId
  public function getFrequency($id)
  {
    if ($id < self::GATEWAY_TRANSACTION_ID_MONTHLY_MIN && $id >= self::GATEWAY_TRANSACTION_ID_ONCE_MIN) { // once
      return self::GATEWAY_FREQUENCY_ONCE;
    } else if ($id >= self::GATEWAY_TRANSACTION_ID_MONTHLY_MIN) { // monthly
      return self::GATEWAY_FREQUENCY_MONTHLY;
    }
  }


  // functions for CRONs

  // get global SQL time
  public function getTime()
  {
    return $this->db->query('SELECT NOW()')->fetchField();
  }

  // get data for reminder
  public function getReminderData($table, $now)
  {
    return $this->db->query('SELECT `transactionId`,`amount`,`giver`,`hash`,`email`,`gateway_personal`.`id`
      FROM `' . $table . '`
      JOIN `gateway_personal` ON `' . $table . '`.`giver` = `gateway_personal`.`id`
      WHERE `' . $table . '`.`timestamp` > DATE_SUB("' . $now . '", INTERVAL 35 MINUTE)
      AND `' . $table . '`.`timestamp` <= DATE_SUB("' . $now . '", INTERVAL 25 MINUTE)
      AND `done` = 0 AND `recieved` = 0;');
  }

  // get bank hash for checking for incoming payments via bank API
  public function getBankHash()
  {
    return $this->db->table($this->table_config)->select('content')->where(['type' => 'bankHash', 'location' => 'config'])->fetchField();
  }

  // check if user promised the payment based on variable symbol (transactionId)
  public function wasPromised(int $id)
  {
    if ($id < self::GATEWAY_TRANSACTION_ID_ONCE_MIN) { // not our variable symbol (< 1000000000)
      return false;
    } else {
      $table = $this->tableMonthlyOrOnce($id);
    }

    return ($this->db->table($table)->select('id')->where('transactionId', $id)->fetchField() != null);
  }

  // check whether we already processed the payment
  public function isAlreadyProcessed($id, $bankTransactionId = '')
  {
    if ($id < self::GATEWAY_TRANSACTION_ID_ONCE_MIN) { // not our variable symbol (< 1000000000) - check also by bankTransactionId
      
      return ($this->db->table($this->table_nopromise)->select('id')->where('bankTransactionId', $bankTransactionId)->fetchField() != null);

    } else {
      $table = $this->tableMonthlyOrOnce($id);
    }

    return ($this->db->table($table)->select('id')->where('transactionId = ? AND recieved != ?', $id, 0)->fetchField() != null);
  }

  // insert promised payment into db
  public function insertPromised(array $payment, array $account)
  {

    $table = $this->tableMonthlyOrOnce($payment['transactionId']);

    $promise = $this->db->table($table)->select('id, giver')->where('transactionId', $payment['transactionId'])->fetch();


    $this->db->query('INSERT IGNORE INTO ' . $this->table_accounts, $account);

    $giverAccount = $this->db->table($this->table_accounts)->select('id')->where(['accountNo' => $account['accountNo'], 'bankCode' => $account['bankCode']])->fetchField();

    $this->db->query('INSERT IGNORE INTO ' . $this->table_personal_accounts, ['giver' => $promise['giver'], 'giverAccount' => $giverAccount]);


    $personal = $this->db->table($this->table_personal)->select('id, email')->where('id', $promise['giver'])->fetch();

    $email = $personal['email'];

    $frequency = $this->getFrequency($payment['transactionId']);

    $data = [
      'amount' => $payment['amount'],
      'currency' => $payment['currency'],
      'transactionId' => $payment['transactionId'],
      'bankTransactionId' => $payment['bankTransactionId'],
      'giver' => $giverAccount,
      'frequency' => $frequency,
      'paymentType' => $payment['paymentType'],
      'promiseId' => $promise['id']
    ];

    $this->db->table($this->table_recieved)->insert($data);

    if ($frequency == self::GATEWAY_FREQUENCY_ONCE) {
      $this->db->table($table)->where('transactionId', $payment['transactionId'])->update(['recieved' => 1]);
    } else {
      $this->db->table($table)->where('transactionId', $payment['transactionId'])->update(['recieved' => $this->getTime()]);
    }

    return ['email' => $email, 'frequency' => $frequency];

  }

  // insert not promised payment
  public function insertNotPromised(array $payment, array $account)
  {

    $this->db->query('INSERT IGNORE INTO ' . $this->table_accounts, $account);

    $giverAccount = $this->db->table($this->table_accounts)->select('id')->where(['accountNo' => $account['accountNo'], 'bankCode' => $account['bankCode']])->fetchField();

    $giverPotential = $this->db->table($this->table_personal_accounts)->select('giver')->where('giverAccount', $giverAccount)->order('id DESC')->limit(1)->fetchField();

    $data = [
      'amount' => $payment['amount'],
      'currency' => $payment['currency'],
      'bankTransactionId' => $payment['bankTransactionId'],
      'recieved' => $this->getTime(),
      'accountId' => $giverAccount,
      'giverPotential' => $giverPotential
    ];

    if (!empty($payment['transactionId'])) $data['vs'] = $payment['transactionId'];
    if (!empty($payment['ss'])) $data['ss'] = $payment['ss'];

    $this->db->table($this->table_nopromise)->insert($data);

  }

}