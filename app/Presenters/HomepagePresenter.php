<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\Repository\GatewayRepository;
use App\Model\Request;

final class HomepagePresenter extends BasePresenter
{

  /** @var Request @inject */
  public $requestService;

  /** @var GatewayRepository @inject */
  public $gatewayRepository;


  public function actionGateway()
  {
    $this->template->info = $this->gatewayRepository->getInfo();
  }

  public function actionPaymentInfo($type = '', $frequency = null, int $vs = 0, $amount = null, int $id = 0, string $hash = '')
  {
    if ($type == 'done') {

      if ($this->gatewayRepository->setAsDone($id, $hash)) {
        $this->template->type = 'done';
      } else {
        $this->template->type = 'errorUnknown';
      }

    } else {

      if ($type == 'transparent') {
        $this->template->info = $this->gatewayRepository->getInfo();

        $this->template->type = 'bank-transfer';
      } else {
        $this->template->info = $this->gatewayRepository->getInfo('darujme');

        if ($frequency == "Jednorázově") $frequency = 'once';
        if ($frequency == 'Trvalý příkaz - měsíčně') $frequency = 'monthly';

        $this->template->type = ($type == 'bank-transfer' && (!($frequency == 'once' || $frequency == 'monthly') || !isset($vs) || !isset($amount))) ? 'failure' : $type;
      }

      if ($this->template->type == 'bank-transfer') {

        if ($this->gatewayRepository->saveFromDarujme($id, $hash, $vs) || $type == 'transparent') {
          $this->template->gatewayInfo = [
            'frequency' => $frequency,
            'vs' => $vs,
            'id' => $id,
            'hash' => $hash,
            'amount' => intval($amount)
          ];
        } else {
          $this->template->type = 'errorUnknown';
        }

      }

    }
  }

}

