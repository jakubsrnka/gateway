<?php


namespace App\Presenters;
use App\Model\Repository\GatewayRepository;
use App\Model\VersionService;
use App\Model\Request;
use Nette;

class BasePresenter extends Nette\Application\UI\Presenter
{
  /** @var VersionService @inject */
  public $versionService;
  
  /** @var GatewayRepository @inject */
  public $gatewayRepository;

  /** @persistent */
  public $backlink = '';

  public function startup()
  {
    $this->template->version = $this->versionService->getVersion();

    parent::startup();
  }


  // Ajax - handle incoming request from gateway
  public function handleGatewayValidate(array $json)
  {

    $this->payload->data = [];

    if ($json) {
      $ip = $this->requestService->getClientAddress();
      $this->payload->data = $this->gatewayRepository->validateData($json, $ip);
    }

    $this->sendPayload();

  }
}
