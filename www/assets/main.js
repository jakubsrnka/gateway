// Document ready...
$(function () {
	$.nette.init();
	bindAutocomplete()
});

function bindAutocomplete () {

	$("input[name='obec']").autocomplete({
		source: function( request, response ) {
			$.ajax( {
				url: "https://ruian.gpcz.eu/naseptavac-obce/" + request.term,
				dataType: "json",
				success: function( data ) {
					response( data.data );
				}
			} );
		},
		minLength: 2,
		select: function( event, ui ) {
			// edit hidden elem
			event.target.nextElementSibling.value = ui.item.id
		}
	} );
}

function isMobileDevice() {
	return window.innerWidth <= 720
}

function cancelEvent(ev){
  window[ev]=function(){null}
}