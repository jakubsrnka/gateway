// initializing all the variables (not always necessary, but it is then nicer and easily debugable in browser console)
var gatewayStepAvailable = 0;
var gatewayFrequency;
var gatewayAmount;
var gatewayPersonalFname;
var gatewayPersonalLname;
var gatewayPersonalPhone;
var gatewayPersonalEmail;
var gatewayPersonalGender;
var gatewayAddressStreet;
var gatewayAddressHouseNumber;
var gatewayAddressObec;
var gatewayAddressObecId;
var gatewayErrored = false;
var gatewayPopupPopped = false;
var gatewayFull = false;

var gatewayData;
var gatewayDoneId;


$(function () {

  // hiding all steps after load
  $('.gateway-main:not(#gatewayMain1)').hide();

  // function for using Return key as btn-next
  $(document).on('keydown', function(e) {
    if (e.key == "Enter") {
      $('.gateway-main:not(#gatewayMain1, #gatewayMain7):visible .btn-next').click();
      if ($('#gatewayMain1').is(':visible')) {
        if ($('#frequency0').prop("checked")) {
          $('#gatewayMain1 .btn-not-yet').click();
        } else {
          $('#gatewayMain1 .btn-next:not(.btn-not-yet)').click();
        }
      }
    }
  });

  // what should happen on .btn-next click
  $('body').on('click', '.gateway-main .btn-next', function() {

    // keeping the user on the page as long as possible
    window.onbeforeunload = function(){return 'Opravdu chcete stránku opustit?'};

    /**
    where should we go on click
      each case of the switch represents one step
    **/
    var gatewayStep = $(this).parents('.gateway-main').attr('data-step');
    switch (gatewayStep) {
      case "1":
        if ($(this).hasClass('btn-not-yet') && !gatewayPopupPopped) { // telling user to support NOT once using the popup
          $('.popup-gateway .popup-inner').hide();
          $('.popup-gateway').show();
          $('.popup-inner[data-step="' + gatewayStep + '"]').show();
          gatewayPopupFitCheck($('.popup-inner[data-step="' + gatewayStep + '"]'));
          gatewayPopupPopped = true;
        } else {
          setTimeout(function() { // input is checked after click, but we need the browser to wait a tiny bit to know the change
            if ($('#frequency0').prop("checked")) {
              gatewayStepOpener(parseInt(gatewayStep) + 1 , 0);
            } else {
              gatewayStepOpener(parseInt(gatewayStep) + 1, 1);
            }
          },100);
        }
        break;
      case "2":
        gatewaySummaryType('amount', $('#frequency0').prop("checked") ? "once" : "monthly");
        gatewayStepOpener(parseInt(gatewayStep) + 2);
        break;
      case "4":
        gatewaySummaryType('personal');
        gatewayStepOpener(parseInt(gatewayStep) + 1);
        break;
      case "5":
        gatewaySummary();
        gatewayStepOpener(parseInt(gatewayStep) + 1, null, false, 0);
        break;
      default:
        gatewayStepOpener(parseInt(gatewayStep) + 1);
    }
  });

  // what should buttons in the popup after first step do
  $('body').on('click', '.popup-gateway .btn-next', function() {
    var gatewayStep = $(this).parents('.popup-inner').attr('data-step');
    $('.popup-gateway').hide();
    if (gatewayStep == "1") {
      setTimeout(function() {
        if ($('#frequency0').prop("checked")) {
          gatewayStepOpener(parseInt(gatewayStep) + 1, 0);
        } else {
          gatewayStepOpener(parseInt(gatewayStep) + 1, 1);
        }
      },10);
    } else {
      gatewayStepOpener(parseInt(gatewayStep) + 1);
    }
  });

  // what should back button do
  $('body').on('click', '.gateway-main .btn-back', function() {
    var gatewayStep = $(this).parents('.gateway-main').attr('data-step');

    // we need to be able to go back even if the gateway errored (in which case we can not go forward)
    gatewayErrored = false;

    /**
    where should we go on click
      each case of the switch represents one step
    **/
    switch (gatewayStep) {
      case "3":
        setTimeout(function() {
          if ($('#frequency0').prop("checked")) {
            gatewayStepOpener(parseInt(gatewayStep) - 1, 0, true);
          } else {
            gatewayStepOpener(parseInt(gatewayStep) - 1, 1, true);
          }
        },10);
        break;
      case "4":
        setTimeout(function() {
          if ($('#frequency0').prop("checked")) {
            gatewayStepOpener(parseInt(gatewayStep) - 2, 0, true);
          } else {
            gatewayStepOpener(parseInt(gatewayStep) - 2, 1, true);
          }
        },10);
        break;
      default:
        gatewayStepOpener(parseInt(gatewayStep) - 1, null, true);
    }
  });

  // we want the buttons "Upravit" to work
  $('body').on('click', '.gateway-main-summary-table > div > a', function() {
    var gatewayStep = $(this).attr('data-step-edit');
    switch(gatewayStep) {
      case "2":
        if ($('#frequency0').prop("checked")) {
          gatewayStepOpener(gatewayStep, 0);
        } else {
          gatewayStepOpener(gatewayStep, 1);
        }
        break;
      case "3":
        if ($('#supportType0').prop("checked")) {
          break;
        } else if ($('#supportType1').prop("checked")) {
          if ($('#frequency0').prop("checked")) {
            gatewayStepOpener(gatewayStep, 0);
          } else {
            gatewayStepOpener(gatewayStep, 1);
          }
        }
      break;
      default:
        gatewayStepOpener(gatewayStep);
    }
  });

  // what should click on the selector on top of the steps do
  $('.gateway-selector-third').click(function() {
    var gatewayStep = $(this).attr('data-step');
    if (gatewayStepAvailable >= parseInt(gatewayStep)) {
      gatewayStepOpener(gatewayStep);
    }
  });

  // if gateway errored before, disable the error on input change
  $('body').on('keydown', '.gateway-form input', function() {
    gatewayErrored = false;
    $(this).prev('label').children('.warning').removeClass('warning');
  });

  // if gateway errored before, disable the error on amount select (either from selectin or own ammount in input)
  $('body').on('click', '.gateway-main-amount-chooser .btn', function() {
    gatewayErrored = false;
    $('.gateway-main-amount-error .required').hide();
  });
  $('body').on('keydown', '.gateway-main-amount-own input', function() {
    gatewayErrored = false;
    $('.gateway-main-amount-error .required').hide();
  });

  // validate gateway data with Ajax
  $('body').on('click', '.gateway-main#gatewayMain7 button', function() {
    cancelEvent('onbeforeunload');
    $('#gatewayMain7 .btn.btn-pink').removeClass('btn-pink').hide();
    $('#gatewayMain7 .dot-windmill-container').show();
    gatewayValidate(JSON.parse(gatewayData));
  });

  // letting user copy the bank account info with a click of a button
  $('body').on('click', '.popup-inner-main-info img', function() {
    $('.popup-inner-main-info img').removeClass('ticked').attr('src', "/assets/icons/copy_icon.svg");
    navigator.clipboard.writeText($(this).prev().children('span').text())
    .then(() => {
      $(this).attr('src', "/assets/icons/check_box_tick.svg");
      $(this).addClass('ticked');
    })
    .catch(err => {
      console.log('Something went wrong', err);
    });
  });

  // asking user whether they are done sending the promised amount
  $('body').on('click', '.popup-gateway-inner-transfer .btn-pink', function(){
    cancelEvent('onbeforeunload');
    $(this).hide();
    $(this).next().show();
  });

});


// function for selector on top of steps
function gatewaySelector(gatewayStep) {
  $('.gateway-selector-third').removeClass('gateway-selector-third-active');
  switch (parseInt(gatewayStep)) {
    case 1:
    case 2:
    case 3:
    $('.gateway-selector-third:nth-child(1)').addClass('gateway-selector-third-active');
    break;
    case 4:
    case 5:
    $('.gateway-selector-third:nth-child(2)').addClass('gateway-selector-third-active');
    gatewayStepAvailable = (gatewayStepAvailable <= 4) ? 4 : gatewayStepAvailable;
    break;
    case 6:
    $('.gateway-selector-third:nth-child(3)').addClass('gateway-selector-third-active');
    gatewayStepAvailable = (gatewayStepAvailable <= 6) ? 6 : gatewayStepAvailable;
    break;
    default:
  }
}

// function for opening steps
function gatewayStepOpener(gatewayStep, gatewaySubstep = null, backwards = false, animate = 250) {

  // if are all inputs full, we can skip to whichever one, but not until then
  if (!gatewayFull) {
    if (gatewaySubstep != null) {
      e = $('.gateway-main[data-step="' + gatewayStep + '"][data-substep="' + gatewaySubstep + '"]');
    } else {
      e = $('.gateway-main[data-step="' + gatewayStep + '"]');
    }
  }

  // not letting go forwards when gateway errored
  if (gatewayErrored && !backwards) return;

  // opening steps itself
  $('.gateway-main').fadeOut(250);
  $('.gateway-main').promise().done(function() {
    if (animate) {
      if (gatewaySubstep != null) {
        $('.gateway-main[data-step="' + gatewayStep + '"][data-substep="' + gatewaySubstep + '"]').fadeIn(animate);
      } else {
        $('.gateway-main[data-step="' + gatewayStep + '"]').fadeIn(animate);
      }
    } else {
      if (gatewaySubstep != null) {
        $('.gateway-main[data-step="' + gatewayStep + '"][data-substep="' + gatewaySubstep + '"]').show();
      } else {
        $('.gateway-main[data-step="' + gatewayStep + '"]').show();
      }
    }
  });
  $("html, body").animate({scrollTop: $(".gateway-selector").offset().top}, 100);
  gatewaySelector(gatewayStep);

  // run the summary function
  if (gatewayStep == "6") gatewaySummary();
}

// if there is something wrong, showing what is wrong
function gatewayError(gatewayStep, gatewayError = null, gatewaySubstep = null) {
  gatewayErrored = true;
  $('.gateway-main').hide();
  if (gatewaySubstep != null) {
    $('.gateway-main[data-step="' + gatewayStep + '"][data-substep="' + gatewaySubstep + '"]').show();
  } else {
    $('.gateway-main[data-step="' + gatewayStep + '"]').show();
  }
  if (gatewayStep == 2) {
    $('.gateway-main-amount-error .required').show();
  } else {
    if (gatewayError) {
      $('#' + gatewayError + 'Label .required').addClass('warning');
      $('#' + gatewayError + 'Label .required').prev('hr').addClass('warning');
    }
  }
}

// summary function - checking if everything is full
function gatewaySummary(level = null) {
  // frequency check
  gatewayFrequency = $('#frequency0').prop("checked") ? "once" : "monthly";

  // amount check
  gatewaySummaryType('amount', gatewayFrequency);

  // address and personal info check
  gatewaySummaryType('address');
  gatewaySummaryType('personal');

  // preparing JSON for backend validation
  gatewayData = '{ "personal" : {"fname":"' + gatewayPersonalFname + '", "lname":"' + gatewayPersonalLname + '", "phone":"' + gatewayPersonalPhone + '", "email":"' + gatewayPersonalEmail + '"},' + 
  '"address" : {"street":"' + gatewayAddressStreet + '","houseNumber":"' + gatewayAddressHouseNumber + '","obecId":"' + gatewayAddressObecId + '","postcode":"' + gatewayAddressPostcode + '","obec":"' + gatewayAddressObec + '"},' + 
  '"payment" : {"frequency":"' + gatewayFrequency + '","amount":"' + gatewayAmount + '"}}';

  // setting the summary for the user
  $('#summaryPersonal span').html(gatewayPersonalFname + " " + gatewayPersonalLname + "<br>" + gatewayPersonalEmail + "<br>" + gatewayPersonalPhone);
  $('#summaryAmount span').html(gatewayAmount + "&nbsp;Kč ");
  $('#summaryAddress span').html(gatewayAddressStreet + " " + gatewayAddressHouseNumber + "<br>" + gatewayAddressObec + ", " + gatewayAddressPostcode);
  $('#summaryFrequency span').html((gatewayFrequency == "monthly") ? "Dlouhodobě" : "Jednorázově");

  // allowing skipping steps back
  gatewayFull = true;
}

// summary function for individual steps
function gatewaySummaryType(level = null, options = "") {
  switch (level) {
    case 'personal':
      gatewayPersonalFname = ($('input#personalFname').val() != "") ? $('input#personalFname').val() : gatewayError(4, 'personalFname');
      gatewayPersonalLname = ($('input#personalLname').val() != "") ? $('input#personalLname').val() : gatewayError(4, 'personalLname');
      gatewayPersonalPhone = ($('input#personalPhone').val().match(/^((\+|00)42[01])?([\s+\-()x]*\d){6,}$/)) ? $('input#personalPhone').val() : gatewayError(4, 'personalPhone');
      gatewayPersonalEmail = ($('input#personalEmail').val().match(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) ? $('input#personalEmail').val() : gatewayError(4, 'personalEmail');
      break;
    case 'address':
      gatewayAddressStreet = ($('input#addressStreet').val() != "") ? $('input#addressStreet').val() : gatewayError(5, 'addressStreet');
      gatewayAddressHouseNumber = ($('input#addressHouseNumber').val().match(/^[eE]?[0-9 /]{1,}[a-zA-Z]?$/) || $('input#addressHouseNumber').val() == "N/A") ? $('input#addressHouseNumber').val() : gatewayError(5, 'addressHouseNumber');
      gatewayAddressObec = ($('input#addressObec').val() != "") ? $('input#addressObec').val() : gatewayError(5, 'addressObec');
      gatewayAddressObecId = ($('input#addressObecId').val() != "") ? $('input#addressObecId').val() : gatewayError(5, 'addressObec');
      gatewayAddressPostcode = ($('input#addressPostcode').val().match(/^[0-9]{3}\ [0-9]{2}|[0-9]{5}$/) || $('input#addressHouseNumber').val() == "N/A") ? $('input#addressPostcode').val() : gatewayError(5, 'addressPostcode');
      break;
    case 'amount':
      gatewayAmount = $('input[name="amount' + options.charAt(0).toUpperCase() + options.slice(1) + '"]:checked').val();
      if (gatewayAmount == "0") {
        gatewayAmount = $('input#amount' + options.charAt(0).toUpperCase() + options.slice(1) + 'Own').val();
      }
      if (gatewayAmount == null || !(parseInt(gatewayAmount) > 0)) {
        if ($('#frequency0').prop("checked")) {
          gatewayError(2, 'amount', 0);
        } else {
          gatewayError(2, 'amount', 1);
        }
      }
      break;
  }
}

// function for current amount
function currentAmount(frequency = '') {
  if (frequency == '') frequency = $('#frequency0').prop("checked") ? "once" : "monthly";
  gatewayAmount = $('input[name="amount' + frequency.charAt(0).toUpperCase() + frequency.slice(1) + '"]:checked').val();
  if (gatewayAmount == "0") {
    gatewayAmount = $('input#amount' + frequency.charAt(0).toUpperCase() + frequency.slice(1) + 'Own').val();
  }
  if (gatewayAmount == null || !(parseInt(gatewayAmount) > 0)) {
    return 0;
  }
  return parseInt(gatewayAmount);
}

// responsivity check (we do not want popups not to fit)
function gatewayPopupFitCheck(e) {
  if (e.height() > $(window).height()) {
    e.css({'top':'0', 'transform':'translateY(0)' });
    if (e.hasClass('popup-gateway-inner-transfer-once') && !isMobileDevice()) {
      e.css({'top':'0', 'transform':'translateY(0) translateX(-15.9375vw)' });
    }
  }
}

// function for backend validation via Ajax
function gatewayValidate (json) {
  $.ajax( {
    url: "?do=gatewayValidate",
    dataType: "json",
    data: {
      json: json
    },
    success: function( data ) {
      gatewayResponse = data.data;

      // if backend sends what we expect, else show error message
      if (gatewayResponse['status'] == 'ok' && gatewayResponse['destination'] == "here") {
        $('.popup-gateway .popup-inner').hide();
        $('.popup-gateway').show();
        $('.popup-inner[data-step="6"][data-substep="' + gatewayResponse['frequency'] + '"]').show();
        gatewayPopupFitCheck($('.popup-inner[data-step="6"][data-substep="' + gatewayResponse['frequency'] + '"]'));
        $('.variable-symbol span').text(gatewayResponse['variableSymbol']);
        $('.popup-amount span').text(gatewayResponse['amount'])
        $('#popupOnceQRCode').attr('src', gatewayResponse['url']);
        $('.popup-gateway-inner-transfer .btn-very-dark-blue a').attr('href', '/darujte/payment-info/done?id=' + gatewayResponse['variableSymbol'] + '&hash=' + gatewayResponse['hash']);
        setTimeout(function(){
          $('.popup-gateway-inner-transfer .btn.btn-light-darker-bluish-gray-transparent').removeClass('btn-light-darker-bluish-gray-transparent').addClass('btn-pink');
        }, 5000);
      } else {
        $('.popup-gateway .popup-inner').hide();
        $('.popup-gateway').show();
        $('.popup-inner[data-step="error"]').show();
        gatewayPopupFitCheck($('.popup-inner[data-step="error"]'));
        cancelEvent('onbeforeunload');
      }
    }
  });
}
